package com.jquiz.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.jquiz.service.DatabaseConnection;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * Member model
 */
public class Member { // TODO: extends BaseModel?
    

    
    public enum Role {
        Regular, Editor
    }
    
    /** Data fields */
    public Integer id;
    public Role role;
    public String username;
    public String email;
    public String firstName;
    public String lastName;
    public String passwordPlain;
    public String passwordSecured;

    protected DatabaseConnection dbc;
    
    public Member() throws SQLException {
        dbc = DatabaseConnection.getInstance();
    }
    
    public static Member findMemberById(Integer id) throws SQLException {
        Member member = null;
        PreparedStatement stmt = DatabaseConnection.getInstance().getConnection()
                .prepareStatement("SELECT id, role, username, email, firstName, lastName, password FROM member WHERE id = ? LIMIT 1");
        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            member = Member.createFromResultSet(rs);
        } else {
            throw new SQLException("Member with Id=\"" + id + "\" not found");
        }
        rs.close();
        stmt.close();
        return member;
    }

    public static Member findMemberByUsername(String username) throws SQLException {
        Member member = null;
        PreparedStatement stmt = DatabaseConnection.getInstance().getConnection()
                .prepareStatement("SELECT id, role, username, email, firstName, lastName, password FROM member WHERE username = ? LIMIT 1");
        stmt.setString(1, username);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            member = Member.createFromResultSet(rs);
        } else {
            throw new SQLException("Member with Username=\"" + username + "\" not found");
        }
        rs.close();
        stmt.close();
        return member;
    }

    public static Member findMemberByEmail(String email) throws SQLException {
        Member member = null;
        PreparedStatement stmt = DatabaseConnection.getInstance().getConnection()
                .prepareStatement("SELECT id, role, username, email, firstName, lastName, password FROM member WHERE email = ? LIMIT 1");
        stmt.setString(1, email);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            member = Member.createFromResultSet(rs);
        } else {
            throw new SQLException("Member with Email=\"" + email + "\" not found");
        }
        rs.close();
        stmt.close();
        return member;
    }
    
    protected static Member createFromResultSet(ResultSet rs) throws SQLException {
        Member member = new Member();
        member.id = rs.getInt(1);
        member.role = Member.Role.valueOf(rs.getString(2));
        member.username = rs.getString(3);
        member.email = rs.getString(4);
        member.firstName = rs.getString(5);
        member.lastName = rs.getString(6);
        member.passwordSecured = rs.getString(7);
        return member;
    }

    public void save() throws SQLException {
        PreparedStatement stmt = dbc.getConnection().prepareStatement("INSERT INTO member (role, username, email, firstName, lastName, password) VALUES (?,  ?,  ?,  ?,  ?,  ?)");
        stmt.setString(1, this.role.toString());
        stmt.setString(2, this.username);
        stmt.setString(3, this.email);
        stmt.setString(4, this.firstName);
        stmt.setString(5, this.lastName);
        stmt.setString(6, DigestUtils.md5Hex(this.passwordPlain));
        stmt.execute();
    }

    public void update() throws SQLException {
        String sql = "UPDATE member SET role = ?, username = ?, email = ?, firstName = ? , lastName = ?";
        if (!this.passwordPlain.equals("")) {
            sql += ", password = ?";
        }
        sql += " WHERE id = ?";
        PreparedStatement stmt = dbc.getConnection().prepareStatement(sql);
        int i = 1;
        stmt.setString(i++, this.role.toString());
        stmt.setString(i++, this.username);
        stmt.setString(i++, this.email);
        stmt.setString(i++, this.firstName);
        stmt.setString(i++, this.lastName);
        if (!this.passwordPlain.equals("")) {
            stmt.setString(i++, DigestUtils.md5Hex(this.passwordPlain));
        }
        stmt.setInt(i, this.id);
        stmt.execute();
    }

    public void delete() throws SQLException {
        String sql = "DELETE FROM member WHERE id = ?";
        PreparedStatement stmt = dbc.getConnection().prepareStatement(sql);
        stmt.execute();
    }
    
}
