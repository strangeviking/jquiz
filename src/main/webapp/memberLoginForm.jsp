<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<jsp:useBean id="form" scope="request" type="com.jquiz.form.LoginForm"/>
<jsp:include page="_header.jsp" />
    <div class="container">
        <form action="${base_url}member/login" method="post" class="form-signin">
            <% if (form.hasError("general")) { %>
                <div class="alert alert-danger">${form.getError("general")}</div>
            <% } %>
            <h2 class="form-signin-heading">Login</h2>
            <div class="form-group<% if (form.hasError("username")) { %> has-error<% } %>">
                <input type="text" id="inputUsername" class="form-control" placeholder="Username" required autofocus maxlength="30" name="username" value="${fn:escapeXml(form.getValue("username"))}">
                <p class="help-block">${form.getError("username")}</p>
            </div>
            <div class="form-group<% if (form.hasError("password")) { %> has-error<% } %>">
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" required maxlength="32" name="password" value="">
                <p class="help-block">${form.getError("password")}</p>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
        </form>
    </div>
<jsp:include page="_footer.jsp" />