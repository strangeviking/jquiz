<footer class="footer">
    <div class="container">
        <p class="text-muted" style="float: left;">&copy; 2015 <a href="${base_url}">JQuiz</a>.</p>
        <p class="text-muted" style="float: right;"><a href="https://bitbucket.org/brevis/jquiz">Bitbucket</a></p>
    </div>
</footer>