<%@ page import="com.jquiz.service.Auth" %>
<jsp:useBean id="page_title" scope="request" type="java.lang.String"/>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="url">${pageContext.request.requestURL}</c:set>
<c:set var="uri" value="${pageContext.request.requestURI}" />
<c:set var="base_url" value="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${pageContext.request.contextPath}/" />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>${page_title}</title>
    <base href="${base_url}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/styles.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="${base_url}"><b>JQuiz</b></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="${base_url}about">About</a></li>
                    <li><a href="${base_url}quiz/list">Explore Quizzes</a></li>
                    <li><a href="${base_url}member/list">TOP Members</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <% if (Auth.isLogged(request.getRequestedSessionId())) { %>
                    <li><a href="${base_url}member/profile"><span class="glyphicon glyphicon-user"></span> <%= Auth.getMemberUsername(request.getRequestedSessionId()) %></a></li>
                    <li><a href="${base_url}member/logout">Logout</a></li>
                    <% } else { %>
                        <li><a href="${base_url}member/login">Login</a></li>
                        <li><a href="${base_url}member/register">Register</a></li>
                    <% } %>
                </ul>
            </div>
        </div>
    </nav>
